import axios, {AxiosResponse} from "axios";
import IProperty from "./interface";

const urlString = document.location.protocol + "//" + document.location.hostname + ":8989/api/v1/test/";
const zillowAPI = axios.create({
    baseURL: urlString,
    headers: {
        "Content-type": "application/json"
    },
    timeout: 5000
});

export default class ZillowDataService{
    getDummyData(): Promise<AxiosResponse>{
        return zillowAPI.get('/Property?access_token=3b48cbf47d3475ab113123631927b61a&$top=5')
    }
    
    getPropertyCircle(longitude: string, latitude: string, radius: number): Promise<AxiosResponse>{
        const tempResult = zillowAPI.get<IProperty>(`/circleSearch?limit=20&latitude=${latitude}&longitude=${longitude}&radius=${radius}`);
        console.log(tempResult)
        return tempResult;
    }
}