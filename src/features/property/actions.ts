import * as types from './types';
import IProperties, {IProperty} from "./interface";

interface PropertyLoading{
    type: typeof types.FETCH_PROPERTY_LOADING
}

interface PropertySuccess{
    type: typeof types.FETCH_PROPERTY_SUCCESS
    payload: IProperty[]
}

interface PropertyFailed{
    type: typeof types.FETCH_PROPERTY_FAILED
}

export type PropertyDispatchTypes = PropertyLoading | PropertySuccess | PropertyFailed;