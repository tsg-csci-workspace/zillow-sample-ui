import {Dispatch} from "redux";
import ZillowDataService from "./api";
import {PropertyDispatchTypes} from "./actions";
import * as types from './types';

const propertyService = new ZillowDataService();
export const getPropertyCircle = (longitude: string, latitude: string, radius: number) => async (dispatch: Dispatch<PropertyDispatchTypes>) =>{
    try{
        dispatch({
            type: types.FETCH_PROPERTY_LOADING
        })

        const res = await propertyService.getPropertyCircle(longitude, latitude, radius);
        dispatch({
            type: types.FETCH_PROPERTY_SUCCESS,
            payload: res.data.bundle
        })
        return Promise.resolve(res.data);
    } catch (error){
        dispatch({
            type: types.FETCH_PROPERTY_FAILED
        })
    }
}