import {createSlice, PayloadAction} from "@reduxjs/toolkit";
import IProperties, {IProperty} from "./interface";

const initialState: IProperties = {
    bundle: [] as IProperty[]
};


export const propertiesSlice = createSlice({
    name: 'properties',
    initialState,
    reducers: {
        FETCH_PROPERTY_SUCCESS: (state,action: PayloadAction<IProperty[]>)=> {
            return {...state, bundle: action.payload};
        }
    }
});

export const {FETCH_PROPERTY_SUCCESS} = propertiesSlice.actions;

export default propertiesSlice.reducer;