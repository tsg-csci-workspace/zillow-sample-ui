import {RootState} from "../../app/store";
import IProperties from "./interface";

export const selectPropertyInformation = (state: RootState) => state.properties as IProperties;
