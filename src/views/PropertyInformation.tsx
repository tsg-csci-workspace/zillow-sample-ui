import {ChangeEvent, useState} from "react";
import {useAppDispatch, useAppSelector} from "../app/hooks";
import {selectPropertyInformation} from "../features/property/selectors";
import {getPropertyCircle} from "../features/property/operations";
import  {IProperty} from "../features/property/interface";

export function PropertyInformation(){
    const properties = useAppSelector(selectPropertyInformation);
    const dispatch = useAppDispatch();
    const [longitude, setLongitude] = useState("");
    const [latitude, setLatitude] = useState("");
    const [radius, setRadius] = useState(0);
    const [isSubmitted, setIsSubmitted] = useState(false);
    //Test push
    const handleLongitudeChange = (e: ChangeEvent<HTMLInputElement>) => {
        setLongitude(e.target.value);
    }

    const handleLatitudeChange = (e: ChangeEvent<HTMLInputElement>) => {
        setLatitude(e.target.value);
    }

    const handleRadiusChange = (e: ChangeEvent<HTMLInputElement>) => {
        setRadius(e.target.valueAsNumber);
    }

    const handleSearchClick = (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        dispatch(getPropertyCircle(longitude, latitude, radius));
        console.log(properties.bundle)
        setIsSubmitted(true);
    }

    const mapProperties = (propertyList: IProperty[]) => {
        return propertyList.map( property => (
            <div key={property.ListingId}>
                <hr/>
                <h3>Street Address: {property.StreetNumber} {property.StreetName}</h3>
                <h3>City/State/Zip: {property.City}, {property.StateOrProvince} {property.PostalCode}</h3>
                <h3>Listing Price: {property.ListPrice}</h3>
                <h3>MLS Status: {property.StandardStatus}</h3>
                <h3>Latitude: {property.Coordinates[1] } Longitude: {property.Coordinates[0]} </h3>
                <h3>Cap Rate: {property.CapRate}</h3>
            </div>
        ));
    }
    return (
        <div>
            <form onSubmit={handleSearchClick}>
                <label>Latitude</label>
                <input type="text" onChange={handleLatitudeChange} value={latitude}/>
                <label>Longitude</label>
                <input type="text" onChange={handleLongitudeChange} value={longitude}/>
                <label>Radius (in miles)</label>
                <input type="number" onChange={handleRadiusChange} value={radius}/>
                <input type="submit" value="search"/>
            </form>
            { isSubmitted ? mapProperties(properties.bundle)
                : <h2>NO DATA PRESENT</h2> }
        </div>
    )
}